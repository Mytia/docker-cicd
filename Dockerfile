# This file is a template, and might need editing before it works on your project.
FROM python:3.6-alpine
ENV PYTHONUNBUFFEREND 1

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.

WORKDIR /code
COPY ./code

RUN pip install -r requirements.txt

# For Django
EXPOSE 8000
CMD sh init.sh python3 manage.py runserver 0.0.0.0:8000
